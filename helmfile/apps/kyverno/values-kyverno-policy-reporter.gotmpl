{{/*
SPDX-FileCopyrightText: 2023 Bundesministerium des Innern und für Heimat, PG ZenDiS "Projektgruppe für Aufbau ZenDiS"
SPDX-License-Identifier: Apache-2.0
*/}}
---
image:
  registry: {{ .Values.global.imageRegistry }}

imagePullSecrets:
{{- range .Values.global.imagePullSecrets }}
  - name: {{ . }}
{{- end }}



metrics:
  enabled: {{ .Values.kubePrometheusStack.enabled }}

ui:
  enabled: true
  image:
    registry: {{ .Values.global.imageRegistry }}
  imagePullSecrets:
  {{- range .Values.global.imagePullSecrets }}
    - name: {{ . }}
  {{- end }}
  ingress:
    enabled: false
    className: {{ .Values.ingress.ingressClassName }}
    hosts:
      - host: {{ .Values.global.hosts.kyverno }}.{{ .Values.global.domain }}
        paths:
          - path: "/"
            pathType: "Prefix"
    tls:
      - secretName: kyverno-policy-reporter-ui-tls
        hosts:
          - {{ .Values.global.hosts.kyverno }}.{{ .Values.global.domain }}
...
